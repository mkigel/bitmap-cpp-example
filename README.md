# bitmap-cpp-example

Generated image:

![image](https://avoronkov.gitlab.io/bitmap-cpp-example/example.bmp)

## Dependencies

[C++ Bitmap Library](https://github.com/ArashPartow/bitmap)
